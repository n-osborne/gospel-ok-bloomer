# Experimenting Gospel on a bloom filter

## Motivations

In this repository, we want to evaluate the usability of the Gospel
tool suite to annotate an OCaml module with logic assertions and to
exploit these annotations through several use cases (checking the
wellformedness of annotations, generating docs with formal
specification, instrumentation, testing, formal verification, etc).
